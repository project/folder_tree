<?php

namespace Drupal\folder_tree\Controller;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Defines FolderTreeController class.
 */
class FolderTreeController extends ControllerBase {
  /**
   * Callback URL handling for folder tree.
   *
   */
  public function folder_tree() {
    $route_name = 'folder_tree.ajax_callback';
    $url = Url::fromRoute($route_name);
    $full_url = $url->toString();

    $build = [
      '#theme' => 'folder_tree_template',
    ];

    $build['#attached']['library'][] = 'folder_tree/folder_tree';
    $build['#attached']['drupalSettings']['ajax_url'] = $full_url;
    $build['#attached']['drupalSettings']['root_url'] = \Drupal::root();

    return $build;
  }

  /**
   * Callback ajax handling for folder tree.
   *
   * @return array
   *   Return markup for the folder tree page.
   */
  public function ajaxCallback() {
    // Get the current request object.
    $request = \Drupal::request();

    // Get a specific request parameter, for example, 'my_parameter'.
    $dir = $request->request->get('dir');

    if($dir) {
      // Instantiate the treeview class.
      $customService = new \Drupal\folder_tree\treeview($dir);

      // Call the custom method.
      $result = $customService->create_tree();

      echo $result;
      die();

    } else {
      throw new BadRequestHttpException('Invalid Request: The request is not valid.');
    }
  }

}

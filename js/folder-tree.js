(function($, Drupal, drupalSettings, once) {
  Drupal.behaviors.color_picker = {
    attach: function(context, settings) {
      once('color_picker', 'body', context).forEach(function (element) {
        $(document).ready( function() {
          $( '#folder-tree-container' ).html( '<ul class="folder-tree start"><li class="wait">' + 'Generating Tree...' + '<li></ul>' );
          var root_url = drupalSettings.root_url;
          getfilelist( $('#folder-tree-container') , root_url);

          function getfilelist( cont, root ) {

            $( cont ).addClass( 'wait' );

            $.post( drupalSettings.ajax_url, { dir: root }, function( data ) {

              $( cont ).find( '.start' ).html( '' );
              $( cont ).removeClass( 'wait' ).append( data );
              if( 'Sample' == root )
                $( cont ).find('ul:hidden').show();
              else
                $( cont ).find('ul:hidden').slideDown({ duration: 500, easing: null });

            });
          }

          $( '#folder-tree-container' ).on('click', 'LI A', function() {
            var entry = $(this).parent();

            if( entry.hasClass('folder') ) {
              if( entry.hasClass('collapsed') ) {

                entry.find('ul').remove();
                getfilelist( entry, escape( $(this).attr('rel') ));
                entry.removeClass('collapsed').addClass('expanded');
              }
              else {

                entry.find('ul').slideUp({ duration: 500, easing: null });
                entry.removeClass('expanded').addClass('collapsed');
              }
            } else {
              $( '#selected_file' ).text( "File:  " + $(this).attr( 'rel' ));
            }
            return false;
          });

        });

      });
    }
  }
}(jQuery, Drupal, drupalSettings, once));

